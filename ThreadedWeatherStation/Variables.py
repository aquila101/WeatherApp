WindSpeedVariable     = 0
GustSpeedVariable     = 0
RainLevelVariable     = 0

WindDirectionVariable = 0
HumidityVariable      = 0
PollutionVariable     = 0

AnemometerArmRadius       = 70*10**(-3)
Pi                        = 3.141592635359
AnemometerCircumference   = 2 * Pi * AnemometerArmRadius

AnemometerCountingPin     = 7
AnemometerWindSpeedPeriod = 10
AnemometerGustSpeedPeriod = 5
ConversionFactorM2Mile    = 1600

RainFallSensorBucketVolume = 0.2794
RainfallSensorHourlyPeriod = 3600
RainfallSensorDailyPeriod  = 86400
