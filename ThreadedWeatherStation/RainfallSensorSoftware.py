import time
import sys
import Variables

'import RPi.GPIO as GPIO'
print ('loading....')

#Setup hardware pins
'GPIO.setmode(GPIO.BOARD)' 
'GPIO.setup(6, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)'

#Set variables
RainfallSensorTipCount = 0
CounterTruthValue       = 1

#These functions compute rainfall by responding to rising edges from the rainfall sensor
#It's all done on a thread to allow more sensors to be addressed in parallel.
def WindspeedCallback(channel):
    global RainfallSensorTipCount
    RainfallSensorTipCount = RainfallSensorTipCount + 1
    
def RainfallFunction(Period):
    
    'GPIO.add_event_detect('
    '    RainfallSensorCountingPin,'
    '    GPIO.RISING,'
    '    callback=WindspeedCallback)' 
    
    while (CounterTruthValue == 1):
        print (str(Period))
        stamp1 = RainfallSensorTipCount
        time.sleep(Period)
        stamp2 = RainfallSensorTipCount
        Variables.RainLevelVariable = RainfallCalculation(stamp1, stamp2, Period)

def RainfallCalculation(stamp1, stamp2, Period):
    calibration         = Variables.RainfallSensorBucketVolume
    NumberOfTips        = (abs(stamp1 - stamp2))
    VolumePerSecond     = (Variables.RainFallSensorBucketVolume * NumberOfTips) / Period
    VolumePerHour       = VolumePerSecond * 3600
    print ("RainfallPerHour: ", VolumePerHour, "mL")

