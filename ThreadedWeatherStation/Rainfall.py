import time
import sys
import Variables

'import RPi.GPIO as GPIO'
print ('loading....')

#Setup hardware pins
'GPIO.setmode(GPIO.BOARD)' 
'GPIO.setup(7, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)'

#Set variables
AnemometerRotationCount = 0
CounterTruthValue       = 1

#These functions compute windspeed by responding to rising edges from the anemometer
#It's all done on a thread to allow more sensors to be addressed in parallel.
def WindspeedCallback(channel):
    global AnemometerRotationCount
    AnemometerRotationCount = AnemometerRotationCount + 1
    
def AnemometerWindSpeed(ArmRadius, Period):
    
    'GPIO.add_event_detect('
    '    AnemometerCountingPin,'
    '    GPIO.RISING,'
    '    callback=WindspeedCallback)' 
    
    while (CounterTruthValue == 1):
        print (str(Period))
        stamp1 = AnemometerRotationCount
        time.sleep(Period)
        stamp2 = AnemometerRotationCount
        Variables.WindSpeedVariable = AnemometerWindSpeedCalculation(stamp1, stamp2, Period)

def AnemometerWindSpeedCalculation(stamp1, stamp2, Period):
    calibration         = 1
    NumberOfRotations   = (abs(stamp1 - stamp2))
    distance_per_second = (Variables.AnemometerCircumference * NumberOfRotations) / Period
    speed_per_hour      = distance_per_second * 3600
    WindSpeed           = speed_per_hour * calibration /Variables.ConversionFactorM2Mile
    print ("Wind speed: ", WindSpeed, "mph")

