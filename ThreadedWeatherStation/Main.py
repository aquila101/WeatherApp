import AnemometerSensorSoftware
import RainfallSensorSoftware

import threading
import Variables
import time

def Printer():
    while True:
        print("Here")
        time.sleep(1)
    return

w1 = threading.Thread(name='AnemometerSpeed', target = AnemometerSensorSoftware.AnemometerWindSpeed, args = (Variables.AnemometerWindSpeedPeriod))
w2 = threading.Thread(name='AnemometerGust' , target = AnemometerSensorSoftware.AnemometerWindSpeed, args = (Variables.AnemometerGustSpeedPeriod))
w3 = threading.Thread(name='RainfallPerHour', target = RainfallSensorSoftware.RainfallFunction,      args = (Variables.RainfallSensorHourlyPeriod))
w4 = threading.Thread(name='RainfallPerDay', target = RainfallSensorSoftware.RainfallFunction,      args = (Variables.RainfallSensorDailyPeriod))


if __name__ == '__main__':
    w1.start()
    w2.start()



